# but 
upload basic flask app to aws 

## basic flaks app
### elastic [beanstalk](https://docs.aws.amazon.com/fr_fr/elasticbeanstalk/latest/dg/create-deploy-python-flask.html)
1. `eb init`  ssh
2. `eb create`
3. `eb open`


### [lightsail](https://aws.amazon.com/getting-started/hands-on/serve-a-flask-app/) (lightsailctl)
create app ->  dockerfile 
1. `aws lightsail create-container-service`
2. `$ aws lightsail create-container-service --service-name flask-service \ --power small --scale 1`
3. `push-container-image`
   1. `--label flask-container --image flask-container`
4. containers.json public-endpoint.json 
5. `create-container-service-deployement`

### [terraform](https://aws.amazon.com/blogs/opensource/automate-python-flask-deployment-to-the-aws-cloud/)
:warning: boto3 & dynamoDB
:warning: volumes

### faire pipeline sur mon serveur
???
### [dev](https://dev.to/aws-builders/dockerize-an-api-based-flask-app-and-deploy-on-amazon-ecs-2pk0).to (pas de compose)
`aws ecr create-repository --repository-name ecs-flask-app`
### tuto basic [compose](https://www.bogotobogo.com/DevOps/Docker/Docker-ECS-Service-Dicsovery-Redis-Flask.php)
### [rds](https://medium.com/@rodkey/deploying-a-flask-application-on-aws-a72daba6bb80)
1. create db 
2. change link in flask app 
3. upload flask 


### deploy flask & react [micro](https://github.com/tleonhardt/flask-react-aws)
### [deploy](https://adamraudonis.medium.com/how-to-deploy-a-website-on-aws-with-docker-flask-react-from-scratch-d0845ebd9da4)
1. frontend 
   1. s3
2. backend 
   1. beanstalk
3. route both


### third 
## add docker-compose
## add class & identification
## add all project


----
# technos 
stockage
- fargate 
  - serverless
- efs
- s3

serv / container
- [ec2](https://openclassrooms.com/fr/courses/4810836-decouvrez-le-cloud-avec-amazon-web-services/7821936-decouvrez-les-services-damazon-ec2)
  - [tuto deploiement](https://openclassrooms.com/fr/courses/4810836-decouvrez-le-cloud-avec-amazon-web-services/7822168-connectez-vous-a-votre-instance)
  - Elastic Block Store
    - disque 
  - Amazon Machine Image
    - SERVEUR pre-config
  - clusters
- [cloudformation](https://openclassrooms.com/fr/courses/2035756-deployez-vos-systemes-et-reseaux-dans-le-cloud-avec-aws/6110321-automatisez-votre-reseau) 
  - automatise l'infra aws (là ou docker automatise l'infra des containers)
  - la création du VPC
    - virtual private cloud
    - ta partie réseau quoi
  - `docker convert`

[simple website](https://openclassrooms.com/fr/courses/4810836-decouvrez-le-cloud-avec-amazon-web-services/7821855-creez-votre-premier-serveur-sur-aws)
- lightsail
  - machine preconfig
- Elastic Beanstalk
  - ec2 [VS](https://repost.aws/fr/knowledge-center/lightsail-considerations-for-use)
  - iam security 

[database](https://openclassrooms.com/fr/courses/4810836-decouvrez-le-cloud-avec-amazon-web-services/7822388-decouvrez-rds) (rather than use redis)

# [intro openclassroom](https://openclassrooms.com/fr/courses/4810836-decouvrez-le-cloud-avec-amazon-web-services)




# cours openclassroom suite
1. simple dockerfile
   1. flask app
   2. `docker build -t testaws .`
   3. `docker run -it -p 8000 testaws `
   4.  tag + push image to DOCKER REGISTRY
      1. `docker build -t testaws .; docker run -it --name testawsapp testaws; docker commit testawsapp testaws:latest; docker tag testaws:latest rorooooooooooooooo/testaws:latest; docker push rorooooooooooooooo/testaws:latest`
   5. TO docker HUB
   6. `docker run -it --name app_uber_api uber_api
docker commit app_uber_api uber_api:latest
docker tag uber_api:latest rorooooooooooooooo/uber_api:latest
docker push rorooooooooooooooo/uber_api:latest`
   7. 
2. add volume
3. add redis db
   1. docker-compose

# deploy task on ecs
you can specify volume



---

# ci/[cd](https://blog.stephane-robert.info/post/introduction-gitlab-ci/#cest-quoi-le-cicd) 

- continuous integration
  - tester chaque modif 
- continuous deploiement
  - toujours dispo
- pipeline
  - contient des tache
    - exec sur des runners  
    - en parallèle  
    - peut pproduire artefact
    - Un artifact peut contenir des fichiers et/ou dossiers qui vont être stockés au sein des pipelines pour être utilisé par d’autres taches.
[simple](https://docs.gitlab.com/ee/ci/quick_start/#gitlab-ciyml-tips)
[complex pipeline](https://docs.gitlab.com/ee/ci/quick_start/tutorial.html)


## runners 
[fac](https://docs.gitlab.com/ee/ci/quick_start/#ensure-you-have-runners-available) [runners](https://gitlab.istic.univ-rennes1.fr/rpantaleon/testaws/-/settings/ci_cd)     
have to [manually](https://docs.gitlab.com/runner/install/linux-manually.html) [install](https://docs.gitlab.com/runner/install/linux-manually.html) & [register](https://docs.gitlab.com/runner/register/) it 


or on official [gitlab](https://gitlab.com/roroooo/testawsofficial/-/jobs/4332971136) works 



---
# deploy to [aws](https://docs.gitlab.com/ee/ci/cloud_deployment/) using ci/cd
1. authenticazte boiths
   1. new iam user
   2. save keys & token in env var 
2. create infra on aws
   1. automate cluster creation using terraform
3. push image to gitlab registry 
   1. `docker build -t registry.gitlab.com/roroooo/testaws .`
   2. `docker push registry.gitlab.com/roroooo/testaws`
4. push files in s3 bucket
   1. [cli](https://aws.amazon.com/fr/cli/) 
   2. [create s3](https://medium.com/@vishal.sharma./create-an-aws-s3-bucket-using-aws-cli-5a19bc1fda79)
   3. `aws config` 
   4. `aws s3api create-bucket --bucket test-bucket-989282 --region us-east-1`
   5. `aws cloudformation create-stack --stack-name testawstack --template-body file://CloudFormation --capabilities CAPABILITY_NAMED_IAM`
   6. `aws deploy push  --application-name testawstack --s3-location s3://stockagetestaws/testaws`
   7. `aws deploy create-deployment --application-name testaws --generate-cli-skeleton`
   8. `aws deploy get-deployment --deployment-id`


template gitlab ????
# deploy to [ecs](https://docs.gitlab.com/ee/ci/cloud_deployment/ecs/deploy_to_aws_ecs.html)